
import Vue from 'vue'

import VueRouter from 'vue-router'
Vue.use(VueRouter)
import router from './router.js'

//导入App根组件#
import app from './App.vue' 

import 'mint-ui/lib/style.css'
import { Header } from 'mint-ui'
Vue.component(Header.name, Header)

import { Button } from 'mint-ui'
Vue.component(Button.name, Button)

import './lib/mui/css/mui.min.css'

var vm = new Vue({
    el:"#app",
    render: c => c(app),
    router
})