import VueRouter from 'vue-router'

import home from './components/center/home.vue'
import msg from './components/center/msg.vue'

var router = new VueRouter({
    routes: [
        {path: '/', redirect: '/home'},
        {path: '/home', component: home},
        {path: '/msg', component: msg}
    ],
    linkActiveClass: 'mui-active'
})

//把路由对象暴露出去
export default router