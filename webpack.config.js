const path = require('path')

//导入 在内存中生成HTML页面的 插件
//只要是插件，都必须放到plugin节点中去
//这个插件的作用：
//1. 自动在内存中根据根据指定页面生成一个内存的页面
//2. 自动把打包好的 bundle.js 追加到页面中去
const htmlWebpackPlugin = require('html-webpack-plugin')

const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports = {
    //entry: path.join('__dirname', './src/main.js'),
    entry: './src/main.js',
    output: {
        //path: path.join('__dirname', './dist'),
        //path: __dirname+'/dist',
        //path: path.resolve('', './dist'),
        filename: 'bundle.js'
    },

    plugins:[
        new htmlWebpackPlugin({
            template: path.join(__dirname, './src/index.html'),
            filename: 'index.html'
        }),
        new VueLoaderPlugin()
    ],
    module:{    //这个节点用于配置所有 第三方模块 加载器
        rules: [
            //配置处理 .css文件的第三方loader规则
            {test: /\.css$/, use: ['style-loader', 'css-loader']},
            {test: /\.scss$/, use: ['style-loader', 'css-loader', 'sass-loader']},
            {test: /\.(ttf|eot|svg|woff|woff2)$/, use: 'url-loader'},
            {test: /\.vue$/, use: 'vue-loader' }

        ]
    },
    resolve:{
        alias:{ //修改Vue被导入时包的路径
            //"vue$": "vue/dist/vue.js"
        }
    }
}

